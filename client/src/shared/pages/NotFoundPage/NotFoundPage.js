import React from 'react';

import SearchBar from '../../components/SearchBar/SearchBar';
import './NotFoundPage.scss';

const NotFoundPage = (props) => {
  return (
    <div>
      <SearchBar history={props.history} />
      <div className='not-found'>
        <p>Parece que la página no existe</p>
      </div>
    </div>
  );
};

export default NotFoundPage;
