import React from 'react';
import _ from 'lodash';

import ClipLoader from '../Loader/Loader';
import ProductsListItem from './ProductsListItem';
import NotFoundProductSearchMsg from './NotFoundProductSearchMsg';
import './ProductList.scss';

const ProductList = (props) => {

  if (props.loading) {
    return <ClipLoader text="Cargando..." />
  }

  if (_.isEmpty(props.products)) {
    return <NotFoundProductSearchMsg />;
  }


  if (typeof document !== 'undefined') {
    const customTittle = `${props.lastCategory} en Mercado Libre Argentina`;
    document.title = customTittle !== document.title ? customTittle : document.title;
  }

  let products = [];
  if (!_.isEmpty(props.products)) {
    products = props.products.map((product) => {
      return <ProductsListItem key={product.id} product={product} />;
    });
  }

  return (
    <div className="container">
      <div className="wrap-list mb-5">
        <div className="container-fluid">
          {products}
        </div>
      </div>
    </div>);
};

export default ProductList;
