import React from 'react';
import { Link } from 'react-router-dom';
import ImgShipping from './../../../assets/ic_shipping.png';

const ProductsListItem = (props) => {
  return (
    <div className="product-list">
      <div className="row">
        <div className="col-12 col-sm-3 col-md-3 col-lg-3">
          <Link to={`/items/${props.product.id}`}>
            <img
              className="rounded img-fluid thumb"
              src={props.product.picture}
              alt='Product'
            />
          </Link>
        </div>
        <div className="col-12 col-sm-6 col-md-6 col-lg-6">
          <div className="contain-detail">
            <div className="info-product">
              <Link to={`/items/${props.product.id}`}>
                <span>{props.product.price.amount}</span>
              </Link>
              <img src={ImgShipping} alt="shipping" />
              <h2>{props.product.title}</h2>
              <h2>Completo Unico!</h2>
            </div>
          </div>
        </div>
        <div className="col-12 col-sm-3 col-md-3 col-lg-3">
          <div className="location">
            <p>{props.product.address}</p>
          </div>
        </div>
      </div>
      <hr />
    </div>

  );

};

export default ProductsListItem;
