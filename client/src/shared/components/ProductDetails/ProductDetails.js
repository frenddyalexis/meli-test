import React from 'react';
import _ from 'lodash';

import ClipLoader from '../Loader/Loader';
import NotFoundProductSearchMsg from '../ProductList/NotFoundProductSearchMsg';
import './ProductDetails.scss';

const ProductDetails = (props) => {
  if (props.loading) {
    return <ClipLoader text="Cargando detalle del producto..." />
  }


  if (_.isEmpty(props.product)) {
    return <NotFoundProductSearchMsg />;
  }

  const description = props.product.description.split('\n').map((item, key) => {
    return (_.isString(item) ? !!_.trim(item) : _.isEmpty(item))
      ? <span key={key}>{item}<br /></span> : <br key={key} />;
  });


  return (
    <div className="container">
      <div className="wrap-list mb-5">
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 col-sm-8 col-md-8 col-lg-8">
              <div className="text-center">
                <img className="img-fluid" src={props.product.picture} alt='Product' />
                <div className="d-block d-sm-none text-left">
                  <h6>{props.product.condition}{props.product.sold_quantity !== 0 ? ` - ${props.product.sold_quantity} vendidos` : ''}</h6>
                  <h4 >{props.product.title}</h4>
                  <h3>{props.product.price.amount}</h3>
                  <button type="button" className="btn btn-primary btn-block">Comprar</button>
                </div>
              </div>
              <h1>Descripcion del Producto</h1>
              <p>{description}</p>
            </div>
            <div className="col-12 col-sm-4 col-md-4 col-lg-4 d-none d-sm-block">
              <h6>{props.product.condition}{props.product.sold_quantity !== 0 ? ` - ${props.product.sold_quantity} vendidos` : ''}</h6>
              <h4>{props.product.title}</h4>
              <h3>{props.product.price.amount}</h3>
              <button type="button" className="btn btn-primary btn-block">Comprar</button>
            </div>
          </div>
        </div>
      </div>
    </div>

  )
};

export default ProductDetails;
