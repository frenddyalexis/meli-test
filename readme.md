# TEST MERCADOLIBRE-FRONTEND

● Cliente:

○ HTML
○ React
○ Sass

● Servidor:

○ Node
○ Express

## Quick Start

- Clone this repo or download it's release archive and extract it somewhere
- You may delete `.git` folder if you get this code via `git clone`

## Usage

Install [nodemon](https://github.com/remy/nodemon) globally

```
npm i nodemon -g
```

Install server and client dependencies

```
yarn or npm install
cd client
yarn or npm install
```

To start the server and client at the same time (from the root of the project)

```
yarn dev
```

# DEPLOY EN HEROKU https://fathomless-coast-74688.herokuapp.com/
